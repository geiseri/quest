import QtQuick 2.0
import Material 0.3 as Material

Item {
    id: trobberMain

    property bool loading
    property string icon
    property int progress

    implicitWidth: trobberMain.loading ? spinner.width : favicon.width

    Behavior on implicitWidth {
        NumberAnimation { duration: 500 }
    }

    Image {
        id: favicon
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: trobberMain.icon
        opacity: trobberMain.loading ?  0 : 1
        height: dp(32)
        width: dp(32)
        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
    }

    Material.ProgressCircle {
        id: spinner
        anchors.centerIn: parent
        height: dp(24)
        width: dp(24)
        indeterminate: false
        minimumValue: 0
        maximumValue: 100
        value: trobberMain.progress
        opacity: trobberMain.loading ?  1 : 0
        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
    }

}

