#ifndef RDPRUNNER_H
#define RDPRUNNER_H

#include <QObject>
#include <QProcess>

class RdpRunner : public QObject
{
    Q_OBJECT
public:
    explicit RdpRunner(QObject *parent = 0);

signals:
    void error(const QString& error);
    void finished(int exitCode);
    void standardError(const QString& error);
    void standardOutput(const QString& output);
    void started();

public slots:

    void run(const QString &program, const QVariantMap &arguments);
    void terminate();

private:
    QProcess *m_rdpProcess;

};

#endif // RDPRUNNER_H
