#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtWebEngine/qtwebengineglobal.h>
#include <QtQml>
#include <QCommandLineParser>
#include "temporaryfile.h"
#include "rdprunner.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setApplicationName("Quest");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("Quest web browser");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("url", "Page to open");

    parser.process(app);

    QString url = QLatin1String("http://stoneware.geekcentral.pub");
    if ( parser.positionalArguments().size() > 0 ) {
        url = parser.positionalArguments().first();
    }

    QtWebEngine::initialize();
    QQmlApplicationEngine engine;

    qmlRegisterType<TemporaryFile>("org.geekcentral.qmlcomponents", 1, 0, "TemporaryFile");
    qmlRegisterType<RdpRunner>("org.geekcentral.qmlcomponents", 1, 0, "RdpRunner");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QMetaObject::invokeMethod(engine.rootObjects().first(), "main", Q_ARG(QVariant, url));

    return app.exec();
}

