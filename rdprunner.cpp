#include "rdprunner.h"
#include <QDebug>

RdpRunner::RdpRunner(QObject *parent) : QObject(parent), m_rdpProcess(new QProcess(this))
{

}

void RdpRunner::run(const QString &program, const QVariantMap &arguments)
{

    QString username = arguments[QLatin1String("Params")].toMap()[QLatin1String("swareUsername")].toString();
    QString domain = arguments[QLatin1String("Params")].toMap()[QLatin1String("swareDomain")].toString();
    QString password = arguments[QLatin1String("Params")].toMap()[QLatin1String("swarePassword")].toString();
    QString port = arguments[QLatin1String("Port")].toString();
    QString server = arguments[QLatin1String("Address")].toString();

}

void RdpRunner::terminate()
{
    m_rdpProcess->terminate();
}
