import QtQuick 2.4
import QtQuick.Layouts 1.1
import Material 0.3 as Material

Material.Card {
    id: toolbarMain
    height: gu(1)
    radius: dp(5)

    property string url
    property alias icon: throbber.icon
    property alias title: urlEditor.helperText
    property alias canGoBack: backButton.enabled
    property bool loading
    property int progress

    onUrlChanged: {
        urlEditor.text = toolbarMain.url
    }

    signal goBack

    RowLayout {
        anchors.fill: parent;
        Material.IconButton {
            id: backButton
            size: dp(32)
            iconName: "awesome/arrow_circle_left"
            onClicked: {
                toolbarMain.goBack();
            }
        }

        Throbber {
            id: throbber
            loading: toolbarMain.loading
            progress: toolbarMain.progress
            Layout.fillHeight: true
        }

        Material.TextField {
            id: urlEditor
            onEditingFinished: {
                toolbarMain.url = text
            }
            Layout.fillHeight: false
            Layout.fillWidth: true
        }
    }
}

