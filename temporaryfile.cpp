#include "temporaryfile.h"

TemporaryFile::TemporaryFile(QObject *parent) :
    QObject(parent),
    m_file(new QTemporaryFile(this))
{
    // Create the file
    m_file->open();
    m_file->close();
}

QString TemporaryFile::path() const
{
    return m_file->fileName();
}

QString TemporaryFile::fileContents() const
{
    QString contents;

    QFile contentsFile(path());

    if ( contentsFile.open(QIODevice::ReadOnly) ) {
        contents = QString::fromUtf8( contentsFile.readAll() );
    }

    return contents;
}
