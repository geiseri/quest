.pragma library

function dumpProperties( obj ) {
    Object.getOwnPropertyNames(obj).sort().forEach(function(val) {
        console.log("Value: %1:(%2) '%3'".arg(val).arg(typeof obj[val]).arg(obj[val]));
    });
}
