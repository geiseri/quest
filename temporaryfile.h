#ifndef TEMPORARYFILE_H
#define TEMPORARYFILE_H

#include <QObject>
#include <QTemporaryFile>

class TemporaryFile : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString path READ path NOTIFY pathChanged)
public:
    explicit TemporaryFile(QObject *parent = 0);

    QString path() const;

signals:
    void pathChanged();

public slots:
    QString fileContents() const;

private:
    QTemporaryFile *m_file;
};

#endif // TEMPORARYFILE_H
