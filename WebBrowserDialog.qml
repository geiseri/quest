import QtQuick 2.5
import QtQuick.Window 2.2
import QtWebEngine 1.2
import Material 0.3 as Material

Material.Window {

    property alias profile: webView.profile

    function openRequest( request ) {
        request.openIn( webView );
    }

    function onDownloadRequested(download) {
        download.accept()
    }

    title: webView.title
    flags: Qt.Dialog
    width: 800
    height: 600
    visible: true
    onClosing: destroy()
    WebEngineView {
        id: webView
        anchors.fill: parent
    }
}
