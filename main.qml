import QtQuick 2.5
import QtQuick.Controls 1.4
import QtWebEngine 1.2
import "Utilities.js" as Utilities

QtObject {
    id: root

    property QtObject settings: QtObject {
        property bool autoLoadImages: true
        property bool javaScriptEnabled: true
        property bool errorPageEnabled: true
        property bool pluginsEnabled: false
        property string homepage: "http://www.google.com"
    }

    property QtObject defaultProfile: WebEngineProfile {
        storageName: "Default"
    }

    property QtObject otrProfile: WebEngineProfile {
        offTheRecord: true
    }

    property Component browserWindowComponent: WebBrowserWindow {
        applicationRoot: root
        onClosing: destroy()
    }
    property Component browserDialogComponent: WebBrowserDialog {
        onClosing: destroy()
    }

    function createWindow(profile) {
        var newWindow = browserWindowComponent.createObject(root,  {"applicationRoot": root, "profile": profile});
        profile.downloadRequested.connect(newWindow.onDownloadRequested);
        return newWindow;
    }

    function createDialog(profile) {
        var newDialog = browserDialogComponent.createObject(root,{"applicationRoot": root, "profile": profile});
        profile.downloadRequested.connect(newDialog.onDownloadRequested);
        return newDialog;
    }

    function main(url) {
        var browserWindow = createWindow(defaultProfile);
        browserWindow.url = url;
        browserWindow.show();
    }
}
