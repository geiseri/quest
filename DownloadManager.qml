import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtWebEngine 1.2
import org.geekcentral.qmlcomponents 1.0

import "Utilities.js" as Utilities

Item {
    id: downloadItem
    property variant download

    signal finished(var id)

    onDownloadChanged: {
        Utilities.dumpProperties(download);

        console.log("type: %1".arg(download.mimeType));
        // application/vnd.client-connection handle as connection

        download.path = downloadFile.path;
        download.stateChanged.connect(downloadItem.onDownloadStateChanged);
        download.receivedBytesChanged.connect(downloadItem.onDownloadProgress);
        download.accept();
    }

    function onDownloadStateChanged() {
        console.log("download state changed %1".arg(download.state));

        if ( download.state === WebEngineDownloadItem.DownloadCompleted ) {
            // open downloadFile.path
            // delete downloadFile.path
            console.log("download done %1".arg(download.path));
            if ( download.mimeType === "application/vnd.client-connection" ) {
                var rdpConfiguration = JSON.parse( downloadFile.fileContents() );
                rdpRunner.run("xfreerdp", rdpConfiguration);
            } else {
                finished(download.id);
            }
        }
    }

    function onDownloadProgress() {
        console.log("download progress changed %1".arg(download.receivedBytes));
    }

    TemporaryFile {
        id: downloadFile
    }

    RdpRunner {
        id: rdpRunner

        onFinished: {
            downloadItem.finished(downloadItem.download.id)
        }
    }
}

