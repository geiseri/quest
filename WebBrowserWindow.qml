import QtQuick 2.5
import QtWebEngine 1.2
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.2
import Material 0.3 as Material
import "Utilities.js" as Utilities

Material.Window {
    id: browserWindow
    property QtObject applicationRoot: null
    property alias profile: webEngineView.profile
    property alias url: webEngineView.url
    property alias icon: webEngineView.icon
    property alias loading: webEngineView.loading
    property alias canGoBack: webEngineView.canGoBack
    property alias progress: webEngineView.loadProgress

    property Component downloadManagerComponent: DownloadManager {}

    property var downloads: Object

    function onDownloadRequested(download) {

        if ( typeof browserWindow.downloads[download.id] === "undefined") {
            browserWindow.downloads[download.id] = downloadManagerComponent.createObject(browserWindow,  {
                                                                                             "download": download
                                                                                         });
            console.log("started %1".arg(download.id));

        }
    }

    function openRequest( request ) {
        request.openIn( webEngineView );
    }


    width: 800
    height: 600
    title: webEngineView.title

    theme {
        primaryColor: Material.Palette.colors["blue"]["500"]
        primaryDarkColor: Material.Palette.colors["blue"]["700"]
        accentColor: Material.Palette.colors["blue"]["A200"]
        backgroundColor: "pink"
        tabHighlightColor: "white"
    }

    ColumnLayout {
        anchors.fill: parent
        WebBrowserToolbar {
            id: toolbar
            Layout.fillWidth: true
            url: browserWindow.url
            icon: browserWindow.icon
            loading: browserWindow.loading
            progress: browserWindow.progress
            title: browserWindow.title
            canGoBack: browserWindow.canGoBack

            onUrlChanged: {
                browserWindow.url = url;
            }

            onGoBack: {
                webEngineView.goBack();
            }
        }

        Material.Card {
            Layout.fillHeight: true
            Layout.fillWidth: true
            WebEngineView {
                id: webEngineView
                focus: true
                anchors.fill: parent

                onFeaturePermissionRequested: {
                    console.log("request: %1 for %2".arg(feature).arg(securityOrigin));
                }

                onFullScreenRequested: {
                    Utilities.dumpProperties(request);

                    browserWindow.showFullScreen();
                }

                onJavaScriptConsoleMessage: {
                    console.log("JS: %1".arg(message));
                }

                onWindowCloseRequested: {
                    console.log("close window");
                    browserWindow.close();
                }

                onNewViewRequested: {
                    var window = null;
                    if( request.destination === WebEngineView.NewViewInDialog) {
                        window = applicationRoot.createDialog(browserWindow.profile);
                    } else {
                        window = applicationRoot.createWindow(browserWindow.profile);
                        window.flags = browserWindow.flags
                        window.height = browserWindow.height
                        window.width = browserWindow.width
                    }
                    window.openRequest(request);
                    window.show();
                }
            }
        }
    }
}
